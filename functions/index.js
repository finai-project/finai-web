const functions = require('firebase-functions');
const cookieParser = require("cookie-parser");
const express = require('express');
const path = require('path');
const bodyParser = require("body-parser");
const admin = require("firebase-admin");
const tf = require("@tensorflow/tfjs");
//Firebase API details
const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://finai-production.firebaseio.com",
    storageBucket: "finai-production.appspot.com/"
  });



//Load routes
var loginRoutes = require("./routes/login.js");
var brokerRoutes = require("./routes/brokers.js");
var commonRoutes = require("./routes/common.js");
var brokerApiRoutes = require('./routes/api/broker/profile.js');
var userApiRoutes = require('./routes/api/users/profile.js');
var brokersApiRoutes = require('./routes/api/brokers/brokers.js');
var finstitutesApiRoutes = require('./routes/api/finstitutes/institutes.js');
var adminRoutes = require("./routes/admin.js");
var loanApplicationApiRoutes = require('./routes/api/loan-application/application.js');
var modelRoutes = require('./routes/api/model/model.js');
const app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.engine("html",require("ejs").renderFile);
app.use(express.static('public'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

async function cookieValidator(req, res, next){
    try{
        const sessionCookie = req.header('Authorization') || req.cookies.__session || "";
        let user = await admin.auth().verifySessionCookie(sessionCookie,true);
        req.verifiedUser = user;
        next();
    }catch{
        next();
    }
}

async function verifyAdmin(req, res, next){
    try{
        let userDoc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        if(userDoc.exists){
            let data = userDoc.data();
            req.isAdmin = data.isAdmin;
        }
        next();
    }catch(e){
        next();
    }
}

app.use( function (req, res, next) {
    req.admin = admin;
    req.db = admin.firestore();
    req.realtimeDB = admin.database();
    return next();
});

app.use(cookieValidator);
app.use(verifyAdmin);
app.get('/', commonRoutes.index);
app.get('/login', brokerRoutes.login);
app.get('/profile', brokerRoutes.profile);
app.get('/dashboard', brokerRoutes.dashboard);
app.get('/register', brokerRoutes.register);
app.get('/initProfile', brokerRoutes.initProfile);
app.post('/finishProfileInit',brokerRoutes.finishInitProfile);
app.get('/signout',brokerRoutes.signOut);
app.get('/admin',adminRoutes.adminPanel);
app.get('/dashboard/client/:id', brokerRoutes.loadClient);
app.post('/sessionLogin', loginRoutes.sessionLogin);
app.get('/api/broker/profile/',brokerApiRoutes.getProfile);
app.post('/api/broker/profile/', brokerApiRoutes.setProfile);
app.get('/api/user/profile/', userApiRoutes.getProfile);
app.post('/api/user/profile/', userApiRoutes.setProfile);
app.get('/api/user/loans', userApiRoutes.getAllLoans);
app.get('/api/brokers',brokersApiRoutes.getBrokers);
app.get('/api/brokers/:id',brokersApiRoutes.getBrokers);
app.get('/api/finstitutes',finstitutesApiRoutes.getInstitutes);
app.get('/api/finstitutes/:id',finstitutesApiRoutes.getInstituteByID);
app.post('/api/finstitutes/',finstitutesApiRoutes.createInstitute);
app.put('/api/finstitutes/:id',finstitutesApiRoutes.updateInstituteByID);
app.delete('/api/finstitutes/:id',finstitutesApiRoutes.deleteInstituteByID);
app.post('/api/loans/application',loanApplicationApiRoutes.createApplication);
app.get('/api/loans/application',loanApplicationApiRoutes.getBrokerApplications);
app.post('/api/loans/application/approve', loanApplicationApiRoutes.approveLoan);
app.post('/api/loans/application/reject', loanApplicationApiRoutes.rejectLoan);
app.post('/api/model/predict', modelRoutes.predict);
app.get('/api/model/predict/:id', modelRoutes.predictID);
app.get('/api/broker/clients', brokerApiRoutes.getClients);
app.get('/about', function(req,res){
    let loggedin = false;
    if(req.verifiedUser){
        loggedin = true;
    }
    res.render("about.pug",{title:"About",admin:req.isAdmin,loggedin:loggedin,nav:"about"});
})

app.get('/contact', function(req,res){
    let loggedin = false;
    if(req.verifiedUser){
        loggedin = true;
    }
    res.render("contact.pug", {title:"Contact", admin:req.isAdmin, loggedin:loggedin, nav:"contact"});
})

app.get('*', function(req, res){
    res.status(404).send('Page not found');
});

exports.app = functions.https.onRequest(app);