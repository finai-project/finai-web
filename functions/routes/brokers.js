const api = require('../routes/api/broker/profile');
const applicationsApi = require('../routes/api/loan-application/application.js');
const brokersApi = require('../routes/api/broker/profile.js');
const usersApi = require('../routes/api/users/profile.js');
const { app } = require('firebase-functions');
exports.profile = async function(req,res){
    if(!req.verifiedUser){
        res.redirect('/login');
    }
    try{
        let profile = null;
        try{
            profile = await api.getProfileCall(req.admin,req.db,req.verifiedUser.user_id);
        }catch{
            res.redirect('/initProfile');
            return;
        }
        if(!profile || !profile.profileInit){
            res.redirect('/initProfile');
            return;
        }
        data = {};
        data.name = profile.name;
        data.email = profile.email;
        data.number = profile.tel;
        data.admin = req.isAdmin;
        data.loggedin = true;
        data.nav = "profile";
        data.title = "Profile Details";
        res.render("profile.pug", data);
    }catch(e){
        res.redirect("/login");
    }
}

exports.initProfile = function(req,res){
    if(req.verifiedUser){
        res.render("initprofile.pug",{title:"Initialize Profile",admin:req.isAdmin,loggedin:true,nav:"profile"});
    }else{
        res.redirect("/login");
    }
}

exports.finishInitProfile = async function(req,res){
    try{
        req.body.email = req.verifiedUser.email;
        let doc = await api.setProfileCall(req.admin,req.db,req.verifiedUser.user_id,req.body);
        res.redirect('/profile');
    }catch(err){
        console.log(err);
        res.redirect('/login');
    }
}

exports.login = function(req, res){
    if(req.verifiedUser){
        res.redirect('/profile');
    }else{
        res.render("login.pug",{loggedin:false,nav:"login",title:"Login"});
    }
};

exports.signOut = function(req,res){
    res.clearCookie('__session');
    res.redirect('/');
}

exports.register = function(req, res){
    res.render("register.pug",{title:"Sign up",nav:"signup"});
};

exports.dashboard = async function(req,res){
    if(!req.verifiedUser){
        res.redirect('/login');
        return;
    }else{
        try{
            let broker_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
            let broker_data = broker_doc.data().broker_profile;
            let applications = await applicationsApi.getBrokerApplicationsCall(req.db, broker_data.uuid);
            let clients = await brokersApi.getClientsCall(req.db,broker_data.uuid);
            console.log(applications);
            console.log(clients);
            res.render("dashboard.pug",{title:"Dashboard", loggedin:true, admin:req.isAdmin, nav:"profile", applications:applications, clients:clients});
        }catch{
            res.render("dashboard.pug",{title:"Dashboard", loggedin:true, admin:req.isAdmin, nav:"profile"});
        }
    }
}

exports.loadClient = async function(req,res){
    let client_id = req.params.id;
    let broker_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
    let broker_data = broker_doc.data().broker_profile;
    let client = await usersApi.getProfileByUUID(req.db,client_id);
    let data = await applicationsApi.getApplicationsFromClientCall(req.db,broker_data.uuid,client_id);
    console.log(client);
    console.log(data);
    res.render("client.pug", {loans:data, client:client});
}