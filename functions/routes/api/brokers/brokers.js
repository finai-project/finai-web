exports.getBrokers = async function (req, res) {
    if (!req.verifiedUser) {
        res.status(403).send('{"error":"Invalid session token"}');
    }
    if ('id' in req.params) {
        let broker = await exports.getBrokerByIDCall(req.admin.req.db, req.params.id);
        res.send(broker);
    }
    try {
        let data = await exports.getBrokersCall(req.admin, req.db);
        res.send(data);
    } catch (e) {
        res.send(e);
    }

}
exports.getBrokerByIDCall = async function (admin, db, id) {
    let query = await db.collection('users').get();
    query.forEach(doc => {
        let fields = doc.data();
        try {
            if ('broker_profile' in fields) {
                let profile = fields.broker_profile;
                if ('uuid' in profile) {
                    if (profile.uuid === id) {
                        let broker = {};
                        broker.name = profile.name;
                        broker.id = profile.uuid;
                        broker.email = profile.email;
                        broker.number = profile.tel;
                        return broker;
                    }
                }
            }
        } catch (e) {

        }
        return {
            error: "No broker with that ID"
        };
    });
}
exports.getBrokersCall = async function (admin, db) {
    let query = await db.collection('users').get();
    let returnJson = {
        brokers: []
    };
    query.forEach(doc => {
        let fields = doc.data();
        try {
            if ('broker_profile' in fields) {
                let profile = fields.broker_profile;
                let broker = {};
                broker.name = profile.name;
                broker.id = profile.uuid;
                broker.email = profile.email;
                broker.number = profile.tel;
                returnJson.brokers.push(broker);
            }
        } catch (e) {

        }
    });
    return returnJson;
}