const tf = require("@tensorflow/tfjs");
exports.predict = async function(req,res){

    tensor = []
    let gender = req.body.gender == "male" ? 1 : 0;
    let married = req.body.married == "true" ? 1 : 0;
    let dependents = parseInt(req.body.dependents)
    let education = req.body.education == "graduate" ? 1 : 0;
    let self_employed = req.body.self_employed == "true" ? 1 : 0;
    let credit_history = parseInt(req.body.credit_history);
    let property_area = 0;
    switch(req.body.property_area){
        case "urban":
            property_area = 0;
            break;
        case "rural":
            property_area = 1;
            break;
        case "semiurban":
            property_area = 2;
            break;
    }
    let income = 0;
    switch(req.body.income){
        case "low":
            income = 0;
            break;
        case "medium":
            income = 1;
            break;
        case "high":
            income = 2;
            break;
    }
    tensor = [[gender,married,dependents,education,self_employed,credit_history,property_area,income]];
    tensor = tf.tensor(tensor);
    let local = req.get('host').includes("localhost")
    let response = await exports.predictCall(local, tensor);
    let col = req.db.collection('predictions');
    await col.doc(makeid(10)).set({status:tensor[0]>0.5});
    res.send(response);
}

exports.predictCall = async function(local, tensorData){
    let url = "";
    if(local){
        url = "http://localhost:5000/model/model.json";
    }else{
        url = "https://finai-production.web.app/model/model.json"
    }
    let model = await tf.loadLayersModel(url);
    let data = tensorData;
    return await(model.predict(data).data());
}

exports.predictID = async function(req,res){
    try{
        let id = req.params.id;
        let doc = await req.realtimeDB.ref('/user/').child(id).once('value');
        let data = doc.val().financial_history;
        let gender = data.gender.toLowerCase() == "male" ? 1 : 0;
        let married = data.married.toLowerCase() == "true" ? 1 : 0;
        let dependents = parseInt(data.dependents)
        let education = data.education.toLowerCase() == "graduate" ? 1 : 0;
        let self_employed = data.self_employed.toLowerCase() == "true" ? 1 : 0;
        let credit_history = parseInt(data.credit_history);
        let property_area = 0;
        switch(data.property_area.toLowerCase()){
            case "urban":
                property_area = 0;
                break;
            case "rural":
                property_area = 1;
                break;
            case "semiurban":
                property_area = 2;
                break;
        }
        let income = 0;
        switch(data.income.toLowerCase()){
            case "low":
                income = 0;
                break;
            case "medium":
                income = 1;
                break;
            case "high":
                income = 2;
                break;
        }
        let tensor = [[gender,married,dependents,education,self_employed,credit_history,property_area,income]];
        tensor = tf.tensor(tensor);
        let local = req.get('host').includes("localhost")
        let response = await exports.predictCall(local, tensor);
        res.send(response);
    }catch{
        res.send("error");
    }
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }