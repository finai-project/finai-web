exports.createApplication = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
    }
    req.body.email = req.verifiedUser.email;
    try{
        let user_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        let user_data = user_doc.data().user_profile;
        let doc = await exports.createApplicationCall(req.db, user_data.uuid, req.body.broker_id, req.body.amount, req.body.period);
        res.send({response:doc});
    }catch(e){
        console.log(e)
        res.status(404).end();
    }
}

exports.approveLoan = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
    }
    try{
        let loan_id = req.body.loan_id;
        console.log(loan_id);
        let broker_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        let broker_data = broker_doc.data().broker_profile;
        let status = await exports.approveLoanCall(req.db,broker_data.uuid,loan_id);
        res.send(status);
    }
    catch(e){
        console.log(e);
        res.status(403).end();
    }
}

exports.approveLoanCall = async function(db, broker_id, loan_id){
    try{

        let doc = await db.collection('loan_application').doc(loan_id);
        let data = await doc.get();
        data = data.data();
        console.log(data.broker_id);
        console.log(broker_id);
        if(data.broker_id == broker_id){
            await doc.set({approved:true},{merge:true});
            return true;
        }else{
            return false;
        }
    }catch(e){
        console.log(e);
        return false;
    }
}

exports.rejectLoan = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
    }
    try{
        let loan_id = req.body.loan_id;
        console.log(loan_id);
        let broker_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        let broker_data = broker_doc.data().broker_profile;
        let status = await exports.rejectLoanCall(req.db,broker_data.uuid,loan_id);
        res.send(status);
    }
    catch(e){
        console.log(e);
        res.status(403).end();
    }
}

exports.rejectLoanCall = async function(db, broker_id, loan_id){
    try{

        let doc = await db.collection('loan_application').doc(loan_id);
        let data = await doc.get();
        data = data.data();
        console.log(data.broker_id);
        console.log(broker_id);
        if(data.broker_id == broker_id){
            await doc.set({rejected:true},{merge:true});
            return true;
        }else{
            return false;
        }
    }catch(e){
        console.log(e);
        return false;
    }
}

exports.getBrokerApplications = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
    }
    try{
        let broker_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        let broker_data = broker_doc.data().broker_profile;
        let applications = await exports.getBrokerApplicationsCall(req.db,broker_data.uuid);
        res.send(applications);
    }
    catch(e){
        console.log(e);
        res.status(403).end();
    }
}

exports.getBrokerApplicationsCall = async function(db, broker_id){
    try
    {
        console.log(broker_id);
        let querySnapshot = await db.collection('loan_application').where('broker_id', '==', broker_id).get();
        let returnArray = [];
        querySnapshot.forEach(doc=>{
            let data = doc.data();
            if(!data.approved && !data.rejected){
                returnArray.push(data);
            }
        });
        return returnArray;
    }
    catch
    {
        return "error";
    }
}

exports.createApplicationCall = async function(db, user_id, broker_id, loan_amount, loan_period){
    try
    {
        let applicationID = "";
        while(true){
            applicationID = makeid(6);
            let doc = await db.collection('loan_application').doc(applicationID).get();
            if(!doc.exists){
                break;
            }
        }
        let data = {
            loan_id: applicationID,
            user_id: user_id.trim(),
            broker_id: broker_id.trim(),
            loan_amount: loan_amount,
            loan_period: loan_period,
            approved: false,
            rejected: false
        }
        let createdDoc = await db.collection('loan_application').doc(applicationID).set(data);
        return data
    }
    catch
    {
        return "error";
    }
}

exports.getApplicationsFromClient = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
    }
    try{
        let client_id = req.body.client_id;
        let broker_doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        let broker_data = broker_doc.data().broker_profile;
        let data = await exports.getApplicationsFromClientCall(req.db,broker_data.uuid,client_id);
        res.send(data);
    }
    catch(e){
        console.log(e);
        res.status(403).end();
    }
}

exports.getApplicationsFromClientCall = async function(db, broker_id, client_id){
    try
    {
        let querySnapshot = await db.collection('loan_application').where('broker_id', '==', broker_id).get();
        let returnArray = [];
        querySnapshot.forEach(doc=>{
            let data = doc.data();
            if(data.user_id == client_id){
                if(data.approved && !data.rejected){
                    returnArray.push(data);
                }
            }
        });
        return returnArray;
    }
    catch
    {
        return "error";
    }
}

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }