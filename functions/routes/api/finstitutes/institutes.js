const { v4: uuidv4 } = require('uuid');

exports.createInstitute = async function(req, res){
    if(!req.verifiedUser || !req.isAdmin){
        res.status(403).send('{"error":"Invalid session token, or not an admin}"');
    }
    try{
        let data = await exports.createInstituteCall(req.admin,req.db,req.body);
        res.send(data);
    }catch(e){
        res.send(e);
    }
}

exports.createInstituteCall = async function(admin,db,body){
    body.uuid = uuidv4();
    let query = await db.collection('finstitutes').doc(body.uuid).set(body);
    return query;
}

exports.getInstitutes = async function(req,res){
    if(!req.verifiedUser){
        res.status(403).send('{"error":"Invalid session token}"');
    }
    try{
        let data = await exports.getInstitutesCall(req.admin,req.db);
        res.send(data);
    }catch(e){
        res.send(e);
    }
}

exports.getInstitutesCall = async function(admin,db){
    let querySnapshot = await db.collection('finstitutes').get();
    let returnArray = [];
    querySnapshot.forEach(doc=>{
        returnArray.push(doc.data());
    });
    return returnArray;
}

exports.updateInstituteByID = async function(req,res){
    if(!req.verifiedUser || !req.isAdmin){
        res.status(403).send('{"error":"Invalid session token}"');
    }
    try{
        let data = await exports.updateInstituteCall(req.admin,req.db,req.body,req.params.id);
        res.send(data);
    }catch(e){
        res.send(e);
    }
}
exports.updateInstituteCall = async function(admin,db,body,id){
    let query = await db.collection('finstitutes').doc(id).set(body);
    query.updatedInstitute = body;
    return query;
}

exports.deleteInstituteByID = async function(req,res){
    if(!req.verifiedUser || !req.isAdmin){
        res.status(403).send('{"error":"Invalid session token}"');
    }
    try{
        let data = await exports.deleteInstituteCall(req.admin,req.db,req.params.id);
        res.send(data);
    }catch(e){
        res.send(e);
    }
}

exports.deleteInstituteCall = async function(admin,db,id){
    let query = await db.collection('finstitutes').doc(id).delete();
    return query;
}

exports.getInstituteByID = async function(req,res){
    if(!req.verifiedUser){
        res.status(403).send('{"error":"Invalid session token}"');
    }
    try{
        let data = await exports.getInstituteByIDCall(req.admin,req.db,req.params.id);
        res.send(data);
    }catch(e){
        res.send(e);
    }

}

exports.getInstitueByIDCall = async function(admin,db,id){
    let query = await db.collection('finstitutes').doc(id).get();
    return query.data();
}