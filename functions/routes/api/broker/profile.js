const { v4: uuidv4 } = require('uuid');

exports.setProfile = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
    }
    req.body.email = req.verifiedUser.email;
    try{
        let doc = await exports.setProfileCall(req.admin,req.db,req.verifiedUser.user_id,req.body);
        res.send({response:"success"});
    }catch{
        res.status(401).end();
    }
}

exports.getProfile = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
        return;
    }
    try{
        let doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        res.send({response:"success", payload:doc.data().broker_profile});
    }catch{
        res.status(401).end();
    }
}

exports.getProfileCall = async function(admin,db,user_id){
    let query = await db.collection('users').doc(user_id).get();
    return query.data().broker_profile;
}

exports.setProfileCall = async function(admin,db,user_id,body){
    body.profileInit = true;
    let uuid = null;
    let doc = await db.collection('users').doc(user_id).get();
    try{
        if(doc.exists){
            let data = doc.data();
            if(data.broker_profile.hasOwnProperty("uuid")){
                uuid = data.broker_profile.uuid;
            }
        }
    }catch{
    }          
    if(uuid == null){
        uuid = uuidv4();
    }      
    body.uuid = uuid;
    let set = await db.collection('users').doc(user_id).set({broker_profile:body},{merge:true});
    return body;
    
}

exports.getClients = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token}"');
        return;
    }
    try{
        let doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        let data = await exports.getClientsCall(req.db,doc.data().broker_profile.uuid);
        res.send(data);
    }catch(e){
        console.log(e);
        res.status(401).end('{"error":"Invalid session token}"');
    }
}

exports.getClientsCall = async function(db, broker_id){
    let clientIDs = [];
    let query = await db.collection('loan_application').where('broker_id','==',broker_id).get();
    query.forEach(doc=>{
        try{
            let data = doc.data();
            if(data.approved){
                clientIDs.push(doc.data().user_id);
            }
        }catch{
            
        }
    });
    console.log(clientIDs);
    let clients = [];
    let userQuery = await db.collection('users').get();
    userQuery.forEach(doc=>{
        try{
            //console.log(doc);
            let client = doc.data().user_profile;
            console.log(client.uuid);
            if(clientIDs.includes(client.uuid)){
                clients.push(client);
            }

        }
        catch(e){
            //console.log(e);
        }
    });
    return clients;
}