const { v4: uuidv4 } = require('uuid');

exports.setProfile = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
        return;
    }

    try{
        req.body.email = req.verifiedUser.email;
        let doc = await exports.setProfileCall(req.admin,req.db,req.verifiedUser.user_id,req.body);
        res.send({response:"success"});
    }catch(e){
        console.log(e);
        res.status(401).end();
    }
}

exports.getProfile = async function(req, res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
        return;
    }
    try{
        let doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        res.send({response:"success", payload:doc.data().user_profile});
    }catch(e){
        console.log(e);
        res.status(401).end();
    }
}

exports.getProfileCall = async function(admin,db,user_id){
    let query = await db.collection('users').doc(user_id).get();
    return query.data().user_profile;
}

exports.getProfileByUUID = async function(db,user_uuid){
    let querySnapshot = await db.collection('users').get();
    let toReturn = null;
    querySnapshot.forEach(doc=>{
        try{
            let data = doc.data();
            if(data.user_profile.uuid == user_uuid){
                toReturn = data.user_profile;
            }
        }
        catch (e){

        }
    });
    return toReturn;

}

exports.setProfileCall = async function(admin,db,user_id,body){
    body.profileInit = true;
    let uuid = null;
    let doc = await db.collection('users').doc(user_id).get();
    try{
        if(doc.exists){
            let data = doc.data();
            if(data.user_profile.hasOwnProperty("uuid")){
                uuid = data.user_profile.uuid;
            }
        }
    }catch{
    }          
    if(uuid == null){
        uuid = uuidv4();
    }      
    body.uuid = uuid;
    let set = await db.collection('users').doc(user_id).set({user_profile:body},{merge:true});
    return body;
    
}

exports.getAllLoans = async function(req,res){
    if(!req.verifiedUser){
        res.status(401).send('{"error":"Invalid session token"}');
        return;
    }
    try{
        let doc = await req.db.collection('users').doc(req.verifiedUser.user_id).get();
        doc = doc.data();
        let response = await exports.getLoansCall(req.admin, req.db, doc.user_profile.uuid);
        res.send(response);
    }catch(e){
        console.log(e);
        res.status(401).end();
    }
}

exports.getLoansCall = async function(admin,db,user_id){
    let querySnapshot = await db.collection('loan_application').where('user_id', '==', user_id).get();
    let returnArray = [];
    querySnapshot.forEach(doc=>{
        let data = doc.data();
        returnArray.push(data);
    });
    return returnArray;
}