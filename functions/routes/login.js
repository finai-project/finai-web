
exports.sessionLogin = function(req,res){
    const idToken = req.body.idToken || "";
    const expiresIn = 60 * 60 * 24 * 5 * 1000;
    req.admin
    .auth()
    .createSessionCookie(idToken, { expiresIn })
    .then((sessionCookie) => {
        const options = { maxAge: expiresIn, httpOnly: true };
        //Set cookie for online users, but also respond back with the cookie itself, for other people using the API
        res.cookie("__session", sessionCookie, options);
        res.send({cookie: sessionCookie, status: "success"});
    },
    (error) => {
        res.status(401).send("UNAUTHORIZED REQUEST!");
    });
}