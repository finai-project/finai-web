exports.adminPanel = async function(req,res){
    if(req.verifiedUser && req.isAdmin){
        const database = req.db;
        try{
            if(req.isAdmin){
                let finstitutes = [];
                let institutesDocRef = await database.collection('finstitutes').get();
                institutesDocRef.forEach(doc=>{
                    finstitutes.push(doc.data());
                });
                res.render("admin.pug",{title:"Admin Panel", loggedin:true, admin:true, nav:"admin", institutes:finstitutes});
            }
        }catch(e){
            console.log(e);
            res.redirect('/');
        }
    }else{
        res.redirect('/');
    }
}