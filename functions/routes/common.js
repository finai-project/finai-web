exports.index = async function(req, res){
    if(!req.verifiedUser){
        res.render("home.pug",{title:"FinAI Web App", nav:"home"});
        return;
    }
    res.render("home.pug",{title:"FinAI Web App",loggedin:true,admin:req.isAdmin,nav:"home"});
};
