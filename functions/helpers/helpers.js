
module.exports = {
    authenticateToken: function (req, sessionCookie) {
        return new Promise(function(resolve, reject){
            req.admin
            .auth()
            .verifySessionCookie(sessionCookie, true /** checkRevoked */)
            .then((decodedClaims) => {
                resolve(decodedClaims);
            })
            .catch((error) => {
                reject(error);
            });
        });
    }
}
