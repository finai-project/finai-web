const firebaseConfig = {
    apiKey: "AIzaSyA0aPXhxAWBELXTnOyqwYv3D_mera-u9Ow",
    authDomain: "finai-production.firebaseapp.com",
    databaseURL: "https://finai-production.firebaseio.com",
    projectId: "finai-production",
    storageBucket: "finai-production.appspot.com",
    messagingSenderId: "1080053406482",
    appId: "1:1080053406482:web:34c9ec2038c8bd0e27f1cb",
    measurementId: "G-955K727LH4"
};

firebase.initializeApp(firebaseConfig);
const auth = firebase.auth();
auth.setPersistence(firebase.auth.Auth.Persistence.NONE);