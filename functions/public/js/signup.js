document.addEventListener("DOMContentLoaded", function(event) { 
    var $form = $('#signupForm');
    var pass = document.getElementById("pass1");
    var confirm = document.getElementById("pass2");
    var emailField = document.getElementById("email");
    var hasTried = false;
    $form.on('submit',(e) => {

        hasTried = true;
        if(pass.value != confirm.value){
            confirm.setCustomValidity("Passwords do not match");
            e.preventDefault();
        }else{
            confirm.setCustomValidity("");
            e.preventDefault();
            var $inputs = $('#signupForm :input');

            // not sure if you wanted this, but I thought I'd add it.
            // get an associative array of just the values.
            var values = {};
            $inputs.each(function() {
                values[this.name] = $(this).val();
            });
            const email = values.email;
            const password = values.password;
            auth.createUserWithEmailAndPassword(email, password)
            .then(({ user }) => {
                return user.getIdToken().then((idToken) => {
                    return fetch("/sessionLogin", {
                        method: "POST",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json",
                            "CSRF-Token": Cookies.get("XSRF-TOKEN"),
                        },
                        body: JSON.stringify({ idToken }),
                    });
                });
            })
            .then(() => {
              return firebase.auth().signOut();
            })
            .then(() => {
              window.location.assign("/profile");
            }).catch((error)=>{
                switch(error.code){
                    case "auth/email-already-in-use":
                        emailField.classList.add('is-invalid');
                }
            });
          return false;
        }
        confirm.reportValidity();
    });
    $('#email').on('input',(e)=>{
        emailField.classList.remove('is-invalid');
    });
    $('#pass2').on('input',(e) => {
        if(hasTried){
            if(pass.value != confirm.value){
                confirm.setCustomValidity("Passwords do not match");
            }else{
                confirm.setCustomValidity("");
            }
            confirm.reportValidity();
        }
    });
});