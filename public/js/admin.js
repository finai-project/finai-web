let doingRequest = false;

$(document).ready(function(){
    var hash = location.hash.substr(1);
    console.log(hash);
    $('#admin a[href="#'+hash+'"]').tab('show');
    $('#admin a').on('click', function (e) {
        $(this).tab('show');
        
    });

    $('#edit button').on('click', function (e){
        e.preventDefault();
        let parent = $(this).parent().parent();
        openEdit(parent);
    });

    $('#delete button').on('click', function (e){
        e.preventDefault();
        let parent = $(this).parent().parent();
        openDeleteConfirm(parent);
    });
    
    $('#editInstituteModal #submitButton').on('click',function(e) {
        e.preventDefault();
        $("form#instituteEditForm").validate();
        if ($('form#instituteEditForm').valid() && !doingRequest){
            doingRequest = true;
            $.ajax({
                url:"api/finstitutes/" + currentInstituteID,
                type: "PUT",
                contentType: 'application/json',
                data: JSON.stringify(getFormData($('form#instituteEditForm').serializeArray())),
                success: function(data){
                    location.reload();
                }
            });
        }
    });
    $('#createInstituteModal #submitButton').on('click',function(e) {
        e.preventDefault();
        $("form#instituteCreateForm").validate();
        if ($('form#instituteCreateForm').valid() && !doingRequest){
            doingRequest = true;
            $.ajax({
                url:"api/finstitutes/",
                type: "POST",
                contentType: 'application/json',
                data: JSON.stringify(getFormData($('form#instituteCreateForm').serializeArray())),
                success: function(data){
                    location.reload();
                }
            });
        }
    });
// set the dimensions and margins of the graph
var width = 450
    height = 450
    margin = 40

// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
var radius = Math.min(width, height) / 2 - margin

// append the svg object to the div called 'my_dataviz'
var svg = d3.select("#pie_chart")
  .append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

// Create dummy data
var data = [0.5,0.5]

// set the color scale
var color = d3.scaleOrdinal()
  .domain(data)
  .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56"])

// Compute the position of each group on the pie:
var pie = d3.pie()
  .value(function(d) {return d.value; })
var data_ready = pie(d3.entries(data))

// Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
svg
  .selectAll('whatever')
  .data(data_ready)
  .enter()
  .append('path')
  .attr('d', d3.arc()
    .innerRadius(0)
    .outerRadius(radius)
  )
  .attr('fill', function(d){ return(color(d.data.key)) })
  .attr("stroke", "black")
  .style("stroke-width", "2px")
  .style("opacity", 0.7)
    

});
let currentInstituteID = "";

function openDeleteConfirm(parent){
    currentInstituteID = parent.attr("id");
    let name = parent.find("#name").text();
    $('#deleteInstituteConfirmation #name').text("Delete " + name + "?")
    $('#deleteInstituteConfirmation').modal();
}

function doDelete(){
    if(doingRequest){
        return;
    }
    doingRequest = true;
    $.ajax({
        url:"api/finstitutes/"+currentInstituteID,
        type: "DELETE",
        success: function(data){
            location.reload();
        }
    });
}

function openEdit(parent){
    currentInstituteID = parent.attr("id");
    let name = parent.find('#name').text();
    let number = parent.find('#number').text();
    let address = parent.find('#address').text();
    $('#editInstituteModal #nameInput').val(name);
    $('#editInstituteModal #addressInput').val(address);
    $('#editInstituteModal #idInput').val(currentInstituteID);
    $('#editInstituteModal #numberInput').val(number);
    $('#editInstituteModal').modal();
}
function getFormData(data) {
    var unindexed_array = data;
    var indexed_array = {};
 
    $.map(unindexed_array, function(n, i) {
     indexed_array[n['name']] = n['value'];
    });
 
    return indexed_array;
 }