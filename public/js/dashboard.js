$(document).ready(function(){
    var hash = location.hash.substr(1);

    $('#sidebar a').on('click', function (e) {
        $(this).tab('show')
        $('#sidebar li').removeClass('active');
        $(this).parent().addClass('active');
    });    
    $('#sidebar a[href="#'+hash+'"]').trigger('click');

    $('.approveButton').on('click',function(e){
        let loanID = $(this).parent().parent().attr('id');
        $.ajax({
            url:"api/loans/application/approve",
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify({loan_id:loanID}),
            success: function(data){
                location.reload();
            }
        });
    });

    $('.rejectButton').on('click',function(e){
        let loanID = $(this).parent().parent().attr('id');
        $.ajax({
            url:"api/loans/application/reject",
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify({loan_id:loanID}),
            success: function(data){
                location.reload();
            }
        });
    });

    $('.viewClient').on('click', function(e){
        let clientID = $(this).parent().attr('id');
        $.ajax({
            url:"dashboard/client/"+clientID,
            type: "GET",
            contentType: 'application/json',
            success: function(data){
                $('#clientPanel').html(data);
                $('#backButton').on('click', function(e){
                    $('#clientPanel').addClass('collapse');
                    $('#clientCards').removeClass('collapse');
                    $('#clientPanel').html("");
                });
                $('#clientPanel').removeClass('collapse');
                $('#clientCards').addClass('collapse');
            }
        });
    });
});