
document.addEventListener("DOMContentLoaded", function(event) { 
  var $form = $('#loginForm');
  var provider = new firebase.auth.GoogleAuthProvider();
  var pass = document.getElementById("pass1");
  var emailField = document.getElementById("email");
  var hasTried = false;
  var googleSignin = $('#googlesignin');
  googleSignin.on('click',function(e){
    firebase.auth().signInWithPopup(provider).then(({ user }) => {
      return user.getIdToken().then((idToken) => {
          console.log(JSON.stringify({idToken}));
          return fetch("/sessionLogin", {
              method: "POST",
              headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  "CSRF-Token": Cookies.get("XSRF-TOKEN"),
              },
              body: JSON.stringify({ idToken }),
          });
      });
  })
  .then(() => {
    return firebase.auth().signOut();
  })
  .then(() => {
    window.location.assign("/profile");
  }).catch((error)=>{
    switch(error.code){
      case "auth/wrong-password":
        console.log("Wrong password");
      default:
        console.log(error);
    }
  });
  });
  $form.on('submit',(e) => {
    e.preventDefault();
    var $inputs = $('#loginForm :input');

    // not sure if you wanted this, but I thought I'd add it.
    // get an associative array of just the values.
    var values = {};
    $inputs.each(function() {
        values[this.name] = $(this).val();
    });
    const email = values.email;
    const password = values.password;
    auth.signInWithEmailAndPassword(email, password)
    .then(({ user }) => {
        return user.getIdToken().then((idToken) => {
            console.log(JSON.stringify({idToken}));
            return fetch("/sessionLogin", {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "CSRF-Token": Cookies.get("XSRF-TOKEN"),
                },
                body: JSON.stringify({ idToken }),
            });
        });
    })
    .then(() => {
      return firebase.auth().signOut();
    })
    .then(() => {
      window.location.assign("/profile");
    }).catch((error)=>{
      switch(error.code){
        case "auth/wrong-password":
          console.log("Wrong password");
        default:
          console.log(error);
      }
    });
  });
  $('#email').on('input',(e)=>{
      emailField.classList.remove('is-invalid');
  });
  $('#pass2').on('input',(e) => {
      if(hasTried){
          if(pass.value != confirm.value){
              confirm.setCustomValidity("Passwords do not match");
          }else{
              confirm.setCustomValidity("");
          }
          confirm.reportValidity();
      }
  });
});